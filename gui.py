import PySimpleGUI as sg
import os
from add_artist_home import edit_mp3s, edit_vorbis
from pprint import pprint

# Blocking window that doesn't close
def gui():
    layout = [[(sg.Text('Select a folder with mp3s or oggs or flacs\'', size=[40, 1]))],
              [sg.FolderBrowse('Browse')],
              [sg.Output(size=(120, 30))],
              [sg.Button('GO', button_color=(sg.YELLOWS[0], sg.BLUES[0])),
               sg.Button('EXIT', button_color=(sg.YELLOWS[0], sg.GREENS[0]))]]


    window = sg.Window('Add hometown information to your music', layout, default_element_size=(30, 2))

    # ---===--- Loop --- #
    while True:
        event, value = window.Read()
        if event == 'GO':
            if value['Browse'] == '':
                print('Browse to a valid folder, bub')
            else:
                dir = value['Browse']
                mp3s = [file for file in os.listdir(dir) if '.mp3' in file]
                vorbs = [file for file in os.listdir(dir) if '.ogg' in file or '.flac' in file]
                pprint('Found these mp3s:  {} and these flacs/oggs:  {}'.format(mp3s, vorbs))
                print('Working...')
                edit_mp3s(mp3s)
                edit_vorbis(vorbs)
                print('Done')
        else:
            break
gui()
