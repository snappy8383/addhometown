import requests
from lxml import html
import mutagen
from mutagen.id3 import TXXX, COMM
import os, re
import logging
logging.getLogger().setLevel(logging.INFO)

#  example
# url = r'https://kunayalaproductions.bandcamp.com/'
# page = requests.get(url)
# tree = html.fromstring(page.content)
# hometown = tree.xpath('//span[@class="title"]/text()')
# secondary = tree.xpath('//span[@class="location secondaryText"]/text()')


def grab_location(url):
    # grab the band location from that page
    page = requests.get(url)
    tree = html.fromstring(page.content)
    return tree.xpath('//span[@class="location secondaryText"]/text()')


def search_bandcamp(mp3):
    # query bandcamp search with the band name from the TPE2 field of an mp3 and use the first result
    tpe2 = mp3.tags.getall("TPE2")
    if tpe2:
        query = r'https://bandcamp.com/search?q={}'.format(mp3.tags.getall("TPE2")[0].text[0])
        page = requests.get(query)
        tree = html.fromstring(page.content)
        homepage = tree.xpath('//li[@class="searchresult band"]//div[@class="itemurl"]//a/text()')[0]
        return homepage
    else:
        logging.error("Could not find a name in the TPE2 field.  Skipping {}".format(tpe2))


def edit_vorbis(vorbs):

    for i in range(len(vorbs)):
        #  load vorb
        vorb = mutagen.File(vorbs[i])
        url = ''
        homepage = ''

        #skip if da 'bis already has a 'hometown' tag  TODO: probably also skip if hometown tag exists but is blank
        if not 'hometown' in vorb.tags.keys():
            #  check if bandcamp url is in the tags somewhere
            comments = ' '.join([v[0] for v in vorb.tags.values()])
            urls = re.findall('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', comments)
            if not urls:
                logging.info('Comments found in {}, but no url in the comments.  Searching bandcamp instead.'.format(
                    vorb.filename))
                # TODO edit search_bandcamp to account for vorbis too.  search = search_bandcamp(vorb)
                search = 0
                if search:
                    logging.info("Found homepage {} for {}".format(search, vorb.filename))
                    url = search
                else:
                    logging.info("Search for homepage failed for {}".format(vorb.filename))
            else:
                url = urls[0]
                logging.info('Found url(s) in the comments {}.  Using the first.'.format(urls))
            if url:
                loc = grab_location(url)
                if loc:
                    logging.info("Applying location {} for {}".format(loc, vorb.filename))
                    vorb.tags.append((str('hometown'), str(loc)))
                    logging.info("Saving {}".format(vorb.filename))
                    vorb.save()
                else:
                    logging.warning("No location found, skipping {}".format(vorb.filename))
        else:
            logging.info('Skipping {} because it already has a hometown tag:  {}'.format(vorb.filename, vorb.tags))


def edit_mp3s(mp3s):

    for i in range(len(mp3s)):
        #  load mp3
        mp3 = mutagen.File(mp3s[i])
        url = ''
        homepage = ''
        #  skip if mp3 already has a hometown set in the TXXX frame
        if not (mp3.tags.getall("TXXX") and [frame for frame in mp3.tags.getall("TXXX") if frame.HashKey == "TXXX:Hometown"]):
            #  check if bandcamp url is in the COMM frame.
            coms = mp3.tags.getall("COMM")
            if coms:
                logging.info('Found COMM frame in {}'.format(mp3.filename))
                com_text = [''.join(i) for i in coms[0].text][0]
                urls = re.findall('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', com_text)
                if not urls:
                    logging.info('COMM frame found in {}, but no url in the comments.  Searching bandcamp instead.'.format(mp3.filename))
                    search = search_bandcamp(mp3)
                    if search:
                        logging.info("Found homepage {} for {}".format(search, mp3.filename))
                        url = search
                    else:
                        logging.info("Search for homepage failed for {}".format(mp3.filename))
                else:
                    url = urls[0]
                    logging.info('Found url(s) in the COMM frame {}.  Using the first.'.format(urls))

            #  otherwise query bandcamp search with the band name from the TPE2 field and use the first match
            else:
                logging.info('no COMM frame in {}.  Searching bandcamp instead'.format(mp3.filename))
                url = search_bandcamp(mp3)

            if url:
                loc = grab_location(url)
                if loc:
                    logging.info("Applying location {} for {}".format(loc, mp3.filename))
                    mp3.tags.add(TXXX(text=loc, desc=u"Hometown"))
                    logging.info("Saving {}".format(mp3.filename))
                    mp3.save()
                else:
                    logging.warning("No location found, skipping {}".format(mp3.filename))
        else:
            logging.info('Skipping {} because it already has a hometown in the TXXX frame:  {}'.format(mp3.filename, mp3.tags.getall('TXXX')))
