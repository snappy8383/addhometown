# Add Hometown

Scans a chosen folder for .mp3, .ogg, or .flac files and, for each file, attempts to add a 'hometown' tag containing information from the band's bandcamp.com page.

